import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
public class TicTacToe extends JPanel{
	
	private static final long serialVersionUID = 1L;
	JButton buttons[]=new JButton[9];
	int change=0;
	public TicTacToe() {
		setLayout(new GridLayout(3,3));
		initializebuttons();
	}
	public void initializebuttons() {
		for(int i=0;i<=8;i++) {
			buttons[i]=new JButton();
			buttons[i].setText("");
			  buttons[i].addActionListener(new buttonListener());
			add(buttons[i]);
		}
	}
	public void resetButtons() {
		for(int i=0;i<=8;i++) {
			buttons[i].setText("");
		}
		change = 0;
	}
	 private class buttonListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			JButton buttonClicked=(JButton)e.getSource();
			if(change%2==0)
				buttonClicked.setText("X");
			else
				buttonClicked.setText("O");
				 
			 if(checkForWin() == true)
	            {
				 if(change%2==0){
					JOptionPane.showConfirmDialog(null, "X wins\nGame Over.");
					resetButtons();
				}
				 else
					 JOptionPane.showConfirmDialog(null, "O wins\nGame Over.");
	                resetButtons();
	            }
			 if(change==9) {
				 JOptionPane.showConfirmDialog(null, "No one wins\nGame Over :(");
				 resetButtons();
				 
				 }
			change++;
		}
		public boolean checkForWin() {
			if(checkAdjacent(0,1)&&checkAdjacent(1,2))
				return true;
			else if(checkAdjacent(3,4)&&checkAdjacent(4,5))
				return true;
			else if(checkAdjacent(6,7)&&checkAdjacent(7,8))
				return true;
			else if ( checkAdjacent(0,3) && checkAdjacent(3,6))
                return true;  
            else if ( checkAdjacent(1,4) && checkAdjacent(4,7))
                return true;
            else if ( checkAdjacent(2,5) && checkAdjacent(5,8))
                return true;
            else if ( checkAdjacent(0,4) && checkAdjacent(4,8))
                return true;  
            else if ( checkAdjacent(2,4) && checkAdjacent(4,6))
                return true;
            else 
                return false;
		}
		public boolean checkAdjacent(int a,int b) {
			if(buttons[a].getText().equals(buttons[b].getText())&&!buttons[a].getText().equals(""))
				return true;
			else
				return false;
		}
	
}
	  
}
