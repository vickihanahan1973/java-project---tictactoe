import javax.swing.JFrame;

public class Driver {
	public static void main(String[] args) 
    {
        JFrame window = new JFrame("Tic-Tac-Toe");
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        window.getContentPane().add(new TicTacToe());
        window.setBounds(3000,200,500,500);
        window.setVisible(true);
    }
}
